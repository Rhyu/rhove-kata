package com.social.rhove.controller;

import com.social.rhove.delegate.ZillowDelegate;
import com.social.rhove.domain.response.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

/**
 * Controller that handles the content served by Zillow.
 */
@RestController
@RequestMapping(path = "zillow")
public class ZillowController extends BaseController {
    ZillowDelegate delegate;

    @Autowired
    public ZillowController(ZillowDelegate delegate) {
        super(ZillowController.class);
        this.delegate = delegate;
    }

    @GetMapping(path = "/search")
    public ServiceResponse searchPropertyDetails(@QueryParam("address") String address, @QueryParam("citystatezip") String citystatezip) throws Exception {
        delegate.propertySearchZillow(address, citystatezip);
        ServiceResponse ret = new ServiceResponse();
        return ret;
    }

    @GetMapping(path = "/deepSearch")
    public ServiceResponse deepSearchPropertyDetail(@QueryParam("address") String address, @QueryParam("citystatezip") String citystatezip) throws Exception {
        delegate.propertyDeepSearchZillow(address, citystatezip);
        ServiceResponse ret = new ServiceResponse();
        return ret;
    }
}
