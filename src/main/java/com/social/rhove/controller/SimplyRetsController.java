package com.social.rhove.controller;

import com.social.rhove.delegate.SimplyRetsDelegate;
import com.social.rhove.domain.request.rets.RetsPropertiesRequest;
import com.social.rhove.domain.response.version.VersionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Controller that handles the content served by Zillow.
 */
@RestController
@RequestMapping(path = "rets")
public class SimplyRetsController extends BaseController {
    SimplyRetsDelegate delegate;

    @Autowired
    public SimplyRetsController(SimplyRetsDelegate delegate) {
        super(SimplyRetsController.class);
        this.delegate = delegate;
    }

    @PostMapping(path = "properties")
    public VersionResponse getProperties(@RequestBody RetsPropertiesRequest request) throws Exception {
        delegate.retrieveTopMLSTargetPropertiesForRequest(request);
        VersionResponse ret = new VersionResponse();
        return ret;
    }
}
