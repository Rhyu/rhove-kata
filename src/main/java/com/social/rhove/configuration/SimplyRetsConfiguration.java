package com.social.rhove.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.config.simplyrets")
@Getter
@Setter
public class SimplyRetsConfiguration {
    private String userName;
    private String password;
    private String baseUrl;

    @Override
    public String toString() {
        return "SimplyRetsConfiguration{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", baseUrl='" + baseUrl + '\'' +
                '}';
    }
}
