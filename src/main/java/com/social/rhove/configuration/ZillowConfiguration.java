package com.social.rhove.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.config.zillow")
@Getter
@Setter
public class ZillowConfiguration {
    private String zwsKey;
    private String baseUrl;

    @Override
    public String toString() {
        return "ZillowConfiguration{" +
                "zwsKey='" + zwsKey + '\'' +
                ", baseUrl='" + baseUrl + '\'' +
                '}';
    }
}
