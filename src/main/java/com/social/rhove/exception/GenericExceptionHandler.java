package com.social.rhove.exception;

import com.social.rhove.domain.response.ServiceResponse;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

public class GenericExceptionHandler extends BaseExceptionHandler<Exception> {
    public GenericExceptionHandler() {
        super(RetsExceptionHandler.class);
    }

    public ResponseEntity<ServiceResponse> handleException(Exception e) {
        e.printStackTrace();
        response.setErrorMessages(Arrays.asList(e.getMessage()));
        return build();
    }
}