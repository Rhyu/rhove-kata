package com.social.rhove.exception;

import com.social.rhove.domain.response.ServiceResponse;
import com.social.rhove.exception.exceptions.RetsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationControllerAdvice extends ResponseEntityExceptionHandler {
    private final static Logger LOGGER = LogManager.getLogger(ApplicationControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ServiceResponse> handleGenericException(Exception ex) {
        GenericExceptionHandler handler = new GenericExceptionHandler();
        return handler.handleException(ex);
    }

    @ExceptionHandler(RetsException.class)
    public ResponseEntity<ServiceResponse> handleRetsException(RetsException ex) {
        RetsExceptionHandler handler = new RetsExceptionHandler();
        return handler.handleException(ex);
    }
}
