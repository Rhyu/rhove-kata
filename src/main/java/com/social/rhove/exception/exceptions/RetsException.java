package com.social.rhove.exception.exceptions;

public class RetsException extends Exception {
    final RetsExceptionType type;

    public RetsException(final RetsExceptionType type) {
        super(type.description);
        this.type = type;
    }

    public int getErrorCode() {
        return type.errorCode;
    }

    public RetsExceptionType getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return type.description;
    }
}
