package com.social.rhove.exception;

import com.social.rhove.domain.response.ServiceResponse;
import com.social.rhove.exception.exceptions.RetsException;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

/**
 * Example Customer Exception Handler to be handle exceptions of type {@link RetsException}
 */
public class RetsExceptionHandler extends BaseExceptionHandler<RetsException> {
    public RetsExceptionHandler() {
        super(RetsExceptionHandler.class);
    }

    public ResponseEntity<ServiceResponse> handleException(RetsException e) {
        e.printStackTrace();
        status = e.getType().getStatusCode();
        response.setErrorCode(Integer.toString(e.getErrorCode()));
        response.setErrorMessages(Arrays.asList(e.getMessage()));
        return build();
    }
}
