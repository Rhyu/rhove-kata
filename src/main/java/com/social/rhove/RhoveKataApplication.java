package com.social.rhove;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RhoveKataApplication {

    public static void main(String[] args) {
        SpringApplication.run(RhoveKataApplication.class, args);
    }

}
