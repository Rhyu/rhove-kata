package com.social.rhove.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseDAO {
    protected Logger LOGGER;

    public BaseDAO(Class rootClass) {
        LOGGER = LogManager.getLogger(rootClass);
    }
}
