package com.social.rhove.domain.request.rets;

import com.social.rhove.domain.request.ServiceRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class RetsPropertiesRequest extends ServiceRequest {
    private String queryString;
    private String status = "active";
    private String type;
    private String subType;
    private String postalCode;

    public Map<String, Object> retrieveQueryStringMap() {
        HashMap<String, Object> queryStringMap = new HashMap<>();
        // Add QueryString (q) if present.
        if (StringUtils.isNotEmpty(queryString)) {
            queryStringMap.put("q", queryString);
        }

        // Add status
        if (StringUtils.isNotEmpty(status)) {
            queryStringMap.put("status", status);
        }

        // Add type
        if (StringUtils.isNotEmpty(type)) {
            queryStringMap.put("type", type);
        }

        // Add subtype
        if (StringUtils.isNotEmpty(subType)) {
            queryStringMap.put("subtype", subType);
        }

        // Add postalCode
        if (StringUtils.isNotEmpty(postalCode)) {
            queryStringMap.put("postalCodes", postalCode);
        }

        // Add idx and count
        queryStringMap.put("idx", "null");
        queryStringMap.put("count", "false");

        return queryStringMap;
    }
}
