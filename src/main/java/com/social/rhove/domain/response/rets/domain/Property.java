package com.social.rhove.domain.response.rets.domain;

import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Property {
    private String roof;
    private String cooling;
    private String style;
    private Integer area;
    private Integer bathsFull;
    private Integer bathsHalf;
    private Integer stories;
    private String flooring;
    private String foundation;
    private String laundryFeatures;
    private String lotDescription;
    private String lotSize;
    private Integer bedrooms;
    private Integer yearBuilt;

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
