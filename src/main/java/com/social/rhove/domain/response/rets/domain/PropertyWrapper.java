package com.social.rhove.domain.response.rets.domain;

import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class PropertyWrapper implements Comparable {
    private String privateRemarks;
    private Property property;
    private String mlsId;
    private String terms;
    private Address address;
    private String agreement;
    private String listDate;
    private Agent agent;
    private String modified;
    private List<String> photos;
    private Integer listPrice;
    private String listingId;
    private MLS mls;
    private Geography geo;
    private String leaseType;
    private String remarks;
    private HOA association;

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof PropertyWrapper) {
            PropertyWrapper compObj = (PropertyWrapper) o;
            DateTime origDT = new DateTime(listDate);
            DateTime compDT = new DateTime(compObj.listDate);
            if (origDT.isBefore(compDT)) {
                return -1;
            } else if (origDT.isAfter(compDT)) {
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }
}
