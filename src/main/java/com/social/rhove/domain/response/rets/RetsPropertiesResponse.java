package com.social.rhove.domain.response.rets;

import com.google.gson.GsonBuilder;
import com.social.rhove.domain.response.ServiceResponse;
import com.social.rhove.domain.response.rets.domain.PropertyWrapper;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class RetsPropertiesResponse extends ServiceResponse {
    List<PropertyWrapper> sortedProperties;

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }
}
