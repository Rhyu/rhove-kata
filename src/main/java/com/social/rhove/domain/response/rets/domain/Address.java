package com.social.rhove.domain.response.rets.domain;

import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Address {
    private String crossStreet;
    private String state;
    private String country;
    private String postalCode;
    private String streetName;
    private String streetNumberText;
    private Integer streetNumber;
    private String city;
    private String full;
    private String unit;

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }
}
