package com.social.rhove.delegate;

import com.social.rhove.configuration.ZillowConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ZillowDelegate extends BaseDelegate {
    private ZillowConfiguration zillowConfiguration;

    @Autowired
    public ZillowDelegate(ZillowConfiguration zillowConfiguration) {
        super(ZillowDelegate.class);
        this.zillowConfiguration = zillowConfiguration;
    }

    public void propertySearchZillow(String address, String citystatezip) {
        LOGGER.info("#propertySearchZillow {} | {}", address, citystatezip);
    }

    public void propertyDeepSearchZillow(String address, String citystatezip) {
        LOGGER.info("#propertyDeepSearchZillow {} | {}", address, citystatezip);
    }
}
