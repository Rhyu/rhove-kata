package com.social.rhove.delegate;

import kong.unirest.JacksonObjectMapper;
import kong.unirest.Unirest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseDelegate {
    protected Logger LOGGER;

    public BaseDelegate(Class rootClass) {
        LOGGER = LogManager.getLogger(rootClass);
        configureUnirest();
    }

    private void configureUnirest() {
        Unirest.config().setObjectMapper(new JacksonObjectMapper());
    }
}
