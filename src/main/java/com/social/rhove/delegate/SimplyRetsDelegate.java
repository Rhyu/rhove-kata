package com.social.rhove.delegate;

import com.social.rhove.configuration.SimplyRetsConfiguration;
import com.social.rhove.domain.request.rets.RetsPropertiesRequest;
import com.social.rhove.domain.response.rets.RetsPropertiesResponse;
import com.social.rhove.domain.response.rets.domain.PropertyWrapper;
import com.social.rhove.exception.exceptions.RetsException;
import com.social.rhove.exception.exceptions.RetsExceptionType;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SimplyRetsDelegate extends BaseDelegate {
    private SimplyRetsConfiguration simplyRetsConfiguration;

    @Autowired
    public SimplyRetsDelegate(SimplyRetsConfiguration simplyRetsConfiguration) {
        super(SimplyRetsDelegate.class);
        this.simplyRetsConfiguration = simplyRetsConfiguration;
    }

    public RetsPropertiesResponse retrieveTopMLSTargetPropertiesForRequest(RetsPropertiesRequest request) throws Exception {
        LOGGER.info("#retrieveTopMLSTargetPropertiesForRequest");
        RetsPropertiesResponse response = new RetsPropertiesResponse();

        // Retrieve properties for request
        List<PropertyWrapper> propertiesForRequest = getPropertiesFromRequest(request);

        // If properties returned are empty throw an exception.
        if (propertiesForRequest.isEmpty()) {
            throw new RetsException(RetsExceptionType.NO_PROPERTIES_RESPONSE);
        }

        // Otherwise, do some basic sorting for ordering the best target properties.
        List<PropertyWrapper> sortedProperties = sortRelevantProperties(propertiesForRequest);

        response.setSortedProperties(sortedProperties);
        return response;
    }

    /**
     * Method that sorts a PropertyWrapper List based on {@link PropertyWrapper#compareTo(Object)}
     *
     * @param propertiesForRequest
     * @return Sorted list of Property Wrappers
     */
    List<PropertyWrapper> sortRelevantProperties(List<PropertyWrapper> propertiesForRequest) {
        return propertiesForRequest.parallelStream()
                                   .sorted()
                                   .collect(Collectors.toList());
    }


    List<PropertyWrapper> getPropertiesFromRequest(RetsPropertiesRequest request) {
        LOGGER.info("#getPropertiesFromRequest");
        PropertyWrapper[] response = Unirest.get("https://api.simplyrets.com/properties")
                                            .queryString(request.retrieveQueryStringMap())
                                            .basicAuth(simplyRetsConfiguration.getUserName(), simplyRetsConfiguration.getPassword())
                                            .asObject(PropertyWrapper[].class)
                                            .getBody();

        return Arrays.asList(response);
    }
}
