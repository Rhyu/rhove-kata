package com.social.rhove;

import com.social.rhove.delegate.DelegateTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DelegateTestSuite.class
})
public class RhoveKataTestSuite {
    /** Empty By Design **/
}
