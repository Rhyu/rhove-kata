package com.social.rhove.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        VersionControllerTests.class
})
public class ControllerTestSuite {
    /** Empty By Design **/
}

