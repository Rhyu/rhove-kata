package com.social.rhove.delegate;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SimplyRetsDelegateTests.class,
        ZillowDelegateTests.class
})
public class DelegateTestSuite {
    /** Empty By Design **/
}

