package com.social.rhove.delegate;

import com.social.rhove.configuration.ZillowConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {ZillowDelegate.class})
@EnableConfigurationProperties(value = {ZillowConfiguration.class})
public class ZillowDelegateTests extends DelegateTestConstants {
    private static final Logger LOGGER = LogManager.getLogger(ZillowDelegateTests.class);
    @Autowired
    ZillowDelegate delegate;

    @Before
    public void setup() {

    }

    @After
    public void testDown() {

    }
}
