package com.social.rhove.delegate;

import com.social.rhove.configuration.SimplyRetsConfiguration;
import com.social.rhove.domain.request.rets.RetsPropertiesRequest;
import com.social.rhove.domain.response.ServiceResponse;
import com.social.rhove.domain.response.rets.RetsPropertiesResponse;
import com.social.rhove.domain.response.rets.domain.PropertyWrapper;
import com.social.rhove.exception.exceptions.RetsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

@SpringBootTest(classes = {SimplyRetsDelegate.class})
@EnableConfigurationProperties(value = {SimplyRetsConfiguration.class})
public class SimplyRetsDelegateTests extends DelegateTestConstants {
    private static final Logger LOGGER = LogManager.getLogger(SimplyRetsDelegateTests.class);
    @Autowired
    SimplyRetsDelegate delegate;

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    /**
     * {@link SimplyRetsDelegate#retrieveTopMLSTargetPropertiesForRequest} Tests
     **/
    @Test
    public void testNoQuery() throws Exception {
        RetsPropertiesRequest request = new RetsPropertiesRequest();
        RetsPropertiesResponse response = delegate.retrieveTopMLSTargetPropertiesForRequest(request);
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(ServiceResponse.SUCCESS);
        assertThat(response.getSortedProperties()).isNotEmpty();
    }

    @Test
    public void testHouston() throws Exception {
        RetsPropertiesRequest request = new RetsPropertiesRequest();
        request.setQueryString("Houston");
        RetsPropertiesResponse response = delegate.retrieveTopMLSTargetPropertiesForRequest(request);
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(ServiceResponse.SUCCESS);
        assertThat(response.getSortedProperties()).isNotEmpty();
    }

    @Test
    public void testNoEntries() throws Exception {
        RetsPropertiesRequest request = new RetsPropertiesRequest();
        request.setQueryString("Columbus");
        try {
            RetsPropertiesResponse response = delegate.retrieveTopMLSTargetPropertiesForRequest(request);
            fail("Should have thrown an exception.");
        } catch (RetsException ex) {
            assertThat(ex.getErrorCode()).isEqualTo(2);
        }

    }

    /**
     * {@link #testSortingWrapperEmpty()} Tests
     */

    @Test
    public void testSortingWrapperEmpty() throws Exception {
        List<PropertyWrapper> listToSort = Arrays.asList();
        List<PropertyWrapper> sortedList = delegate.sortRelevantProperties(listToSort);
        assertThat(sortedList).isEmpty();
    }

    @Test
    public void testSortingWrapperSingleElement() throws Exception {
        List<PropertyWrapper> listToSort = Arrays.asList(
                new PropertyWrapper().setListDate("2011-05-23T18:50:30.184391Z")
        );
        List<PropertyWrapper> sortedList = delegate.sortRelevantProperties(listToSort);
        assertThat(sortedList).isNotEmpty();
        assertThat(sortedList.size()).isEqualTo(1);
    }

    @Test
    public void testSortingWrapperDuplicateElements() throws Exception {
        List<PropertyWrapper> listToSort = Arrays.asList(
                new PropertyWrapper().setListDate("2011-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2012-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2011-05-23T18:50:30.184391Z")
        );
        List<PropertyWrapper> sortedList = delegate.sortRelevantProperties(listToSort);
        assertThat(sortedList).isNotEmpty();
        assertThat(sortedList).isInOrder();
        for (PropertyWrapper pw : sortedList) {
            LOGGER.info(pw.getListDate());
        }
    }

    @Test
    public void testSortingWrapperMultipleElementsTest() throws Exception {
        List<PropertyWrapper> listToSort = Arrays.asList(
                new PropertyWrapper().setListDate("2015-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2011-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2012-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2013-05-23T18:50:30.184391Z"),
                new PropertyWrapper().setListDate("2014-05-23T18:50:30.184391Z")
        );
        List<PropertyWrapper> sortedList = delegate.sortRelevantProperties(listToSort);
        assertThat(sortedList).isNotEmpty();
        assertThat(sortedList).isInOrder();
        for (PropertyWrapper pw : sortedList) {
            LOGGER.info(pw.getListDate());
        }
    }
}
