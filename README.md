<h1>Rhove Kata</h1>
<h3>Preface:</h3>
The purpose of this kata is to provide a sample implementation for a potential use case to benefit Rhove.  During our preliminary discussion there it was mentioned that a primary lead gathering technique is to have renters request and introduce the landlord(s) to Rhove.  I thought a potential other primary lead generator could be utilizing data on how long rental properties have been vacant for, and use that as a potential sales pitch to attract new renters to that property.
<br>
With that goal I initially went down Zillow since they had a listed API and are well-known, but the information I required, while listed on their site was not available by their API.  People have circumvented this by screen scraping but I felt that was a bit ineligant for a kata, so after much research I found a comperable API -- SimplyRets.  SimplyRets allows you to inject MLS credentials and consume the dataset by a list of available API's that helped me get ot my goal.  But since I don't possess MLS credentials I just utilized their sample dataset they provide.

<h3>Implementation:</h3>
I created a SpringREST/Spring Boot API that could act as scaffolding for an expansive MLS inquiry service, but the primary implementation for the kata was done through SimplyRets.  I created REST endpoints that would take input parameters from a user like SearchQuery and Postal Code to call SimplyRet's Properties endpoint and do some basic handling on the response to prioritize what properties would best be candidates for contact (Order by latest listing date that is still active).
<br>
Additionally I have the scaffolding set up for the Zillow API (Including an API key) to use their deep property search functionality, initially I was going to use additional data on Zillow to further refine the SimplyRets dataset, but in regards to time I'll turn this in as is (Still might accomplish this later since I spent a fair bit of time exploring opportunities in Zillow).
<h3>Prerequisites:</h3>
<h5>Confirming Installations</h5>

```
#Java 8
➜ java --version
openjdk 8.x.x 2019-04-16   #Needs to be 8, Oracle or OpenJDK are fine.
OpenJDK Runtime Environment (build 8.x.x)
OpenJDK 64-Bit Server VM (build 8.x.x.x, mixed mode, sharing)
```

```
#Maven
➜ mvn --version
Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T14:41:47-04:00)
Maven home: /Users/*****/ext/apache-maven-3.6.0
Java version: 12.0.1, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk-12.0.1.jdk/Contents/Home
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.13.6", arch: "x86_64", family: "mac"
```
<h5>Requirement Downloads</h5>
<ul>
    <li><a href="https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html">Java 8</a></li>
    <li><a href="https://maven.apache.org/download.cgi">Maven</a></li>
</ul>


<h3>How to Run:</h3>

```java
➜ mvn spring-boot:run
   or
➜ mvn package && java -jar target/kata-0.0.1-SNAPSHOT.jar
```